
attribute vec3 a_position;
attribute vec2 a_texCoord;

uniform mat4 u_worldTransform;
uniform mat4 u_viewTransform;
uniform mat4 u_perspective;
uniform vec4 u_transform;


varying vec2 v_texCoord;;

void main() {
  v_color = a_color;

  gl_Position = vec4(a_position, 1.0);
  v_texCoord = a_texCoord;
}
