
attribute vec3 a_position;
attribute vec4 a_color;
/* uniform vec4 u_transform; */
varying vec4 v_color;

void main() {
  v_color = a_color;
  /* gl_Position = a_position + u_transform; */
  gl_Position = vec4(a_position, 1.0);
}


