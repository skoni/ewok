#version 120

attribute vec3 a_position;
attribute float a_radius;


void main() {
  gl_Position = vec4(a_position, 1.0);
  gl_PointSize = a_radius;
}
