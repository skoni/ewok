#version 120

attribute vec2 position;
attribute float radius;


void main() {
  gl_Position = vec4(position, 0.0, 1.0);
  gl_PointSize = radius;
}
