
(c-declare #<<END

#include "Log.h"
#include "Application.h"
#include "Scene.h"
#include "Node.h"
#include "List.h"

using namespace ewok;

END
)


;; ############################################################
;; c-define-types
;; ############################################################

(c-define-type App* (pointer "Application"))
(c-define-type Scene* (pointer "Scene"))

(c-define-type Node* (pointer "Node"))

;; ############################################################
;; c-lambdas
;; ############################################################

(define log-info
  (c-lambda (char-string) void "LogInfo(___arg1);"))

;; application ------------------------------------------------

(define app-scene
  (c-lambda (App*) Scene* "___result_voidstar = ___arg1->scene();"))

;; scene ------------------------------------------------------

(define scene-rootnode
  (c-lambda (Scene*) Node* "___result_voidstar = ___arg1->rootNode();"))

;; node -------------------------------------------------------

(define create-node
  (c-lambda () Node* "___result_voidstar = new Node();"))

(define node-x
  (c-lambda (Node*) float "___result = ___arg1->transform.position.x;"))
(define node-y
  (c-lambda (Node*) float "___result = ___arg1->transform.position.y;"))
(define node-z
  (c-lambda (Node*) float "___result = ___arg1->transform.position.z;"))

(define node-x!
  (c-lambda (Node* float) void "___arg1->transform.position.x = ___arg2;"))
(define node-y!
  (c-lambda (Node* float) void "___arg1->transform.position.y = ___arg2;"))
(define node-z!
  (c-lambda (Node* float) void "___arg1->transform.position.z = ___arg2;"))

(define node-rotx
  (c-lambda (Node*) float "___result = ___arg1->transform.rotation.x;"))
(define node-roty
  (c-lambda (Node*) float "___result = ___arg1->transform.rotation.y;"))
(define node-rotz
  (c-lambda (Node*) float "___result = ___arg1->transform.rotation.z;"))

(define node-rotx!
  (c-lambda (Node* float) void "___arg1->transform.rotation.x = ___arg2;"))
(define node-roty!
  (c-lambda (Node* float) void "___arg1->transform.rotation.y = ___arg2;"))
(define node-rotz!
  (c-lambda (Node* float) void "___arg1->transform.rotation.z = ___arg2;"))

(define node-add-child
  (c-lambda (Node* Node*) void "___arg1->addChild(___arg2);"))

(define node-remove-child
  (c-lambda (Node* Node*) void "___arg1->removeChild(___arg2);"))


;; list -------------------------------------------------------

;; (define node-)

;; ############################################################
;; c-defines
;; ############################################################


;; game -------------------------------------------------------

(c-define (on-init app) (App*) void "EwokBridgeInit" ""
	  (main-on-init app))
	  
(c-define (on-update app time) (App* double) void "EwokBridgeUpdate" ""
	  (main-on-update app time))
	  
(c-define (on-deinit app) (App*) void "EwokBridgeDeinit" ""
	  (main-on-deinit app))




;; (on-mouse-event)
;; (on-key-event)

