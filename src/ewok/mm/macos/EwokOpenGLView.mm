#include "EwokOpenGLView.h"


#include "Log.h"


// static displaylink callback which calls back into the opengl view
static CVReturn DisplayLinkCallback(CVDisplayLinkRef displayLink,
				    const CVTimeStamp *now,
				    const CVTimeStamp *outputTime,
				    CVOptionFlags flagsIn,
				    CVOptionFlags *flagsOut,
				    void *displayLinkContext)
{
  CVReturn result = [(EwokOpenGLView *)displayLinkContext getFrameForTime:outputTime];
  return result;
}


@implementation EwokOpenGLView
- (id)initWithFrame:(NSRect)frameRect
     andApplication:(ewok::Application*)application
{
  app = application;
  
  // create and set the opengl pixelformat
  NSOpenGLPixelFormatAttribute attribs[] = {
    NSOpenGLPFADoubleBuffer,
    NSOpenGLPFAAccelerated,
    NSOpenGLPFAColorSize, 32,
    NSOpenGLPFADepthSize, 32,
    0
  };

  NSOpenGLPixelFormat *pixelFormat = [[NSOpenGLPixelFormat alloc]
				      initWithAttributes: attribs];

  if (!pixelFormat) {
    ewok::LogError("Cannot create OpenGL pixelformat");
  }
  
  self = [super initWithFrame:frameRect pixelFormat:pixelFormat];
  if (!self) {
    ewok::LogError("OpenGL view could not be initialized.");
  }
  [pixelFormat release];
  [[self openGLContext] makeCurrentContext];

  // aquire displaylink callback
  GLint swapInt = 1;
  [[self openGLContext] setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];

  CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
  CVDisplayLinkSetOutputCallback(displayLink, &DisplayLinkCallback, self);

  CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];
  CGLPixelFormatObj cglPixelFormat = (CGLPixelFormatObj)[[self pixelFormat] CGLPixelFormatObj];
  CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,
						    cglContext,
						    cglPixelFormat);

  NSSize bounds = [self frame].size;
  app->init(bounds.width, bounds.height);

  CVDisplayLinkStart(displayLink);
  
  return self;
}

-(void) dealloc {
  CVDisplayLinkStop(displayLink);
  CVDisplayLinkRelease(displayLink);

  [super dealloc];
}




- (CVReturn)getFrameForTime:(const CVTimeStamp *)outputTime {
  // There is no pool, because the method is called from a background thread
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  
  CGLContextObj cglContext = (CGLContextObj)[[self openGLContext] CGLContextObj];
  CGLLockContext(cglContext);

  [[self openGLContext] makeCurrentContext];

  // ----
  double timeFreq = CVGetHostClockFrequency();
  double hostTime = (double) outputTime->hostTime;
  double now = hostTime / timeFreq;

  // NSSize bounds = [self frame].size;
  app->update(now);
  // ----
  
  CGLUnlockContext(cglContext);
  
  [[self openGLContext] flushBuffer];

  [pool release];
  
  return kCVReturnSuccess;
}


-(void) windowWillClose: (NSNotification *)note {
  [NSApp terminate: self];
}
@end
