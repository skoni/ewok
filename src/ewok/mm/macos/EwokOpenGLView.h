#ifndef SCONIOPENGLVIEW_H_
#define SCONIOPENGLVIEW_H_

#import <Cocoa/Cocoa.h>
#import <CoreVideo/CoreVideo.h>

#include "Application.h"


using namespace ewok;


@interface EwokOpenGLView: NSOpenGLView <NSWindowDelegate>
{
  Application *app;
  CVDisplayLinkRef displayLink;
}
- (id)initWithFrame:(NSRect)frameRect andApplication:(Application*)application;

- (CVReturn)getFrameForTime:(const CVTimeStamp *)outputTime;

@end




#endif  // SCONIOPENGLVIEW_H_
