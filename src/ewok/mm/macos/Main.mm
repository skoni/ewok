#include <stdio.h>
#include <stdlib.h>


#include "GambitUtil.h"
#include "Application.h"
#include "Log.h"

#import <Cocoa/Cocoa.h>
#import "EwokOpenGLView.h"


static const char *kEwokAppName = "RailRunner";


int main(int argc, char *argv[])
{
  ewok::LogInfo("========================================");
  ewok::LogInfo("             %s", kEwokAppName);
  ewok::LogInfo("========================================");
  
  ewok::GambitInit();

  ewok::Application app;

  // Cocoa window and view
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  [NSApplication sharedApplication];
  NSRect frame = NSMakeRect(0, 0, 640, 480);

  NSWindow *window = [[NSWindow alloc]
		      initWithContentRect: frame
		      styleMask: NSTitledWindowMask | NSClosableWindowMask
		      | NSMiniaturizableWindowMask
		      backing: NSBackingStoreBuffered
		      defer: false];

  NSString *title = [NSString stringWithUTF8String:kEwokAppName];
  [window setTitle: title];
  [title release];
  
  NSView *view = [[EwokOpenGLView alloc] initWithFrame:frame andApplication:&app];
  [window setContentView: view];
  [window setDelegate: view];
  [window makeKeyAndOrderFront: nil];

  [NSApp run];

  [view release];
  [pool release];
  // ---
  
  app.deinit();

  ewok::GambitDeinit();
  
  return 0;
}
