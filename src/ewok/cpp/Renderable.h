#ifndef RENDERABLE_H_
#define RENDERABLE_H_


namespace ewok {


class Renderable {
public:
  virtual ~Renderable() = 0;
  virtual void render(double time) = 0;
};



} // namespace ewok


#endif  // RENDERRENDERABLE_H_
