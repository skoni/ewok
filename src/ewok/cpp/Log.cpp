#include "Log.h"


#include <cstdarg>
#include <cstdio>


namespace ewok {


enum LogLevel {
  LOG_INFO,
  LOG_WARN,
  LOG_ERROR
};


static const char *LogPrefix[] = {
  "INFO:  ",
  "WARN:  ",
  "ERROR: "
};


void LogMessage(LogLevel level, const char *msg, va_list args) {
  printf("%s", LogPrefix[level]);
  vprintf(msg, args);
  printf("\n");
  fflush(stdout);
}


void LogInfo(const char *msg, ...) {			
  va_list args;
  va_start(args, msg);
  LogMessage(LOG_INFO, msg, args);
  va_end(args);
}


void LogWarn(const char *msg, ...) {			
  va_list args;
  va_start(args, msg);
  LogMessage(LOG_WARN, msg, args);
  va_end(args);
}


void LogError(const char *msg, ...) {			
  va_list args;
  va_start(args, msg);
  LogMessage(LOG_ERROR, msg, args);
  va_end(args);
}


} // namespace ewok
