#ifndef IMAGE_H_
#define IMAGE_H_

#include <cstddef>


namespace ewok {


class Image {
 public:
  Image();
  virtual ~Image();

  void load(const char *filename);
  
  unsigned width() const;
  unsigned height() const;
  unsigned numComponents() const;
  
  unsigned size() const;

  unsigned char *pixels() const;

 private:
  int width_;
  int height_;
  int components_;
  
  size_t size_;
  unsigned char *pixels_;
};


} // namespace ewok

#endif  // IMAGE_H_
