#ifndef LOG_H_
#define LOG_H_

namespace ewok {

void LogInfo(const char *msg, ...);
void LogWarn(const char *msg, ...);
void LogError(const char *msg, ...);

} // namespace ewok

#endif  // LOG_H_
