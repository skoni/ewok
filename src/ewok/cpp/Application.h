#ifndef APPLICATION_H_
#define APPLICATION_H_


#include "Scene.h"
#include "Renderer.h"


namespace ewok {


class Application {
 public:
  Application();
  
  void init(int width, int height);
  void deinit();
  
  void update(double time);

  Scene *scene();

 private:
  Scene scene_;
  Renderer renderer_;

  unsigned fps_;
  double lastTime_;
};


} // namespace ewok

#endif  // APPLICATION_H_
