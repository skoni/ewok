#ifndef LIST_H_
#define LIST_H_

#include <cassert>
#include "Log.h"


namespace ewok {


template<typename T> struct ListNode;
template<typename T> class ListIter;


// -----------------------------------------------------------------------------
// List
// -----------------------------------------------------------------------------

template<typename T>
class List {
  friend class ListIter<T>;
  
public:
  List();
  List(const List<T> &list);
  virtual ~List();

  List<T>& operator=(const List<T> &list);

  T& append(const T &data);

  void remove(const T &data);
  void removeAll();

  T first() const;
  T last() const;
  
  unsigned length() const;

  ListIter<T> iter();
  const ListIter<T> iter() const;
  
private:
  void appendNode(ListNode<T> *node);
  
  ListNode<T> *head_;
  ListNode<T> *tail_;

  unsigned length_;
};


template<typename T>
List<T>::List()
  : head_(NULL),
    tail_(NULL),
    length_(0) {
}


template<typename T>
List<T>::List(const List<T> &list)
  : head_(NULL),
    tail_(NULL),
    length_(0)
{
  ListIter<T> i = list.iter();
  while(i.moveNext()) {
    append(i.current());
  }
}


template<typename T>
List<T>& List<T>::operator=(const List<T> &list) {
  ListIter<T> i = list.iter();
  while(i.moveNext()) {
    append(i.current());
  }
  return *this;
}


template<typename T>
List<T>::~List() {
  removeAll();
}


template<typename T>
T& List<T>::append(const T &data) {
  ListNode<T> *node = new ListNode<T>(data, NULL, tail_);
  appendNode(node);
  return node->data;
}


template<typename T>
void List<T>::appendNode(ListNode<T> *node) {
  if (!head_) {
    head_ = node;
  }
  if (tail_) {
    tail_->next = node;
  }
  tail_ = node;
  ++length_;
}


template<typename T>
T List<T>::first() const {
  assert(head_);
  return head_->data;
}


template<typename T>
T List<T>::last() const {
  assert(tail_);
  return tail_->data;
}


template<typename T>
void List<T>::remove(const T &data) {
  ListIter<T> i = iter();
  while(i.moveNext()) {
    if (i.current() == data) {
      i.remove();
      return;
    }
  }
  LogWarn("List::remove data not found");
}


template<typename T>
void List<T>::removeAll() {
  ListNode<T> *node = head_;
  ListNode<T> *next;
  
  while(node) {
    next = node->next;
    delete node;
    node = next;
  }
  head_ = tail_ = NULL;
  length_ = 0;
}



template<typename T>
inline
ListIter<T> List<T>::iter() {
  return ListIter<T>(this);
}


template<typename T>
inline
const ListIter<T> List<T>::iter() const {
  return ListIter<T>(this);
}


template<typename T>
inline
unsigned List<T>::length() const {
  return length_;
}


// ----------------------------------------------------------------------
// ListNode
// ----------------------------------------------------------------------

template<typename T>
struct ListNode {
  T data;
  ListNode<T> *next;
  ListNode<T> *prev;

  ListNode(const T &data, ListNode<T> *next, ListNode<T> *prev);
  ListNode(ListNode<T> *next, ListNode<T> *prev);
};


template<typename T>
ListNode<T>::ListNode(const T &d, ListNode<T> *n, ListNode<T> *p)
  : data(d),
    next(n),
    prev(p) {
}


template<typename T>
ListNode<T>::ListNode(ListNode<T> *n, ListNode<T> *p)
: next(n),
  prev(p) {
}


// ----------------------------------------------------------------------
// ListIter
// ----------------------------------------------------------------------


template<typename T>
class ListIter {
  friend class List<T>;

public:
  bool moveNext() const;
  bool movePrev() const;
  
  T& current() const;

  void reset() const;

  T remove();
  
private:
  ListIter(List<T> *list) : list_(list), current_(NULL) {}

  ListIter(const List<T> *list) : list_(NULL), current_(NULL) {
    list_ = const_cast< List<T>* >(list);
  }

  List<T> *list_;
  mutable ListNode<T> *current_;
};


template<typename T>
inline
bool ListIter<T>::moveNext() const {
  if (!current_) {
    current_ = list_->head_;
    return current_ != NULL;
  }
  else if (current_->next == NULL) {
    return false;
  }
  else {
    current_ = current_->next;
    return true;
  }
}


template<typename T>
inline
bool ListIter<T>::movePrev() const {
  if (current_ == NULL) {
    current_ = list_->tail_;
    return current_ != NULL;
  }
  else if (current_->prev == NULL) {
    return false;
  }
  else {
    current_ = current_->prev;
    return true;
  }
}


template<typename T>
inline
T& ListIter<T>::current() const {
  assert(current_ != NULL);
  return current_->data;
}


template<typename T>
void ListIter<T>::reset() const {
  current_ = NULL;
}


template<typename T>
inline
T ListIter<T>::remove() {
  ListNode<T> *node = current_;

  if (node == list_->head_) {
    list_->head_ = node->next;
  }
  else {
    node->prev->next = node->next;
  }

  if (node == list_->tail_) {
    list_->tail_ = node->prev;
  }
  else {
    node->next->prev = node->prev;
  }

  current_ = node->next ? node->next : node->prev;
  T data = node->data;
  
  delete node;
  --list_->length_;

  return data;
}


} // namespace ewok


#endif  // LIST_H_
