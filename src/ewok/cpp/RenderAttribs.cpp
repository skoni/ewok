#include "RenderAttribs.h"


#include <cassert>


namespace ewok {


// -----------------------------------------------------------------------------
// RenderAttrib
// -----------------------------------------------------------------------------


RenderAttrib::RenderAttrib()
  : name(NULL),
    compType(RenderAttrib::FLOAT),
    numComps(0),
    compSize(0),
    index(0) {
}


RenderAttrib::RenderAttrib(const char *name,
			   RenderAttrib::Type compType,
			   unsigned numComps,
			   unsigned index)
  : name(name),
    compType(compType),
    numComps(numComps),
    index(index) {
  switch (compType) {
  case BYTE:
  case UNSIGNED_BYTE: compSize = sizeof(char); break;
  case SHORT:
  case UNSIGNED_SHORT: compSize = sizeof(short); break;
  case INT:
  case UNSIGNED_INT: compSize = sizeof(int); break;
  case FLOAT:  compSize = sizeof(float); break;
  case DOUBLE: compSize = sizeof(double); break;
  }
}


// -----------------------------------------------------------------------------
// RenderAttribs
// -----------------------------------------------------------------------------


RenderAttribs::RenderAttribs()
  : nextIndex_(0),
    stride_(0)
{
}


RenderAttribs::~RenderAttribs() {
}


void RenderAttribs::append(const char *name, RenderAttrib::Type compType, unsigned numComps) {
  RenderAttrib attrib(name, compType, numComps, nextIndex_);
  // attribs_.append(attrib);
  attribs_.push_back(attrib);
  ++nextIndex_;
}


void RenderAttribs::bindLocations(unsigned programId) {
  assert(!stride_);

  std::vector<RenderAttrib>::iterator it;
  RenderAttrib attrib;
  
  for(it = attribs_.begin(); it != attribs_.end(); it++) {
    glBindAttribLocation(programId, it->index, it->name);
    stride_ += it->compSize * it->numComps;
  }
}


void RenderAttribs::enable() {
  std::vector<RenderAttrib>::iterator it;

  for(it = attribs_.begin(); it != attribs_.end(); it++) {
    glEnableVertexAttribArray(it->index);
  }

  int offset = 0;
  
  for(it = attribs_.begin(); it != attribs_.end(); it++) {
    glVertexAttribPointer(it->index, it->numComps, it->compType,
			  GL_FALSE, stride_, (const void *)offset);
    offset += it->numComps * it->compSize;
  }
  
  CheckGLError();
}


void RenderAttribs::disable() {
  std::vector<RenderAttrib>::iterator it;
  for(it = attribs_.begin(); it != attribs_.end(); it++) {
    glEnableVertexAttribArray(it->index);
  }
  
  CheckGLError();
}


} // namespace ewok
