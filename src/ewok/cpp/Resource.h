#ifndef RESOURCE_H_
#define RESOURCE_H_


namespace ewok {

class Resource {
public:
  Resource();
  virtual ~Resource();

  virtual void init(const char *uri);
  const char *uri();
  
protected:
  const char *uri_;
};


Resource::Resource() {
}


Resource::~Resource() {
}


void init(const char *uri) {
  uri_ = uri;
}


const char *Resource::uri() {
  return _uri;
}


} // namespace ewok




#endif  // RESOURCE_H_
