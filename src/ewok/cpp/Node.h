#ifndef NODE_H_
#define NODE_H_

#include <list>
#include "Matrix43.h"


namespace ewok {


class Scene;


// -----------------------------------------------------------------------------
// NodeContent
// -----------------------------------------------------------------------------


class Node;


class NodeContent {
  friend class Node;

protected:
  Node* node();
  
private:
  void setNode(Node *node);
  Node* node_;
};


// -----------------------------------------------------------------------------
// NodeTransform
// -----------------------------------------------------------------------------

class NodeTransform {
 public:
  Vector3 position;
  Vector3 rotation;

  const Matrix43 update(const Matrix43 &parent);
  const Matrix43 transform();

 private:
  Matrix43 transform_;
};


// -----------------------------------------------------------------------------
// Node
// -----------------------------------------------------------------------------


class Node {
  friend class Scene;
  
 public:
  Node(NodeContent *content=NULL);
  ~Node();

  bool operator ==(const Node &other);

  Node *parent() const;

  void addChild(Node *node);
  void removeChild(Node *node);
  const std::list<Node*> *childs();

  bool visible();
  void setVisible(bool vis);

  NodeTransform transform;

 private:
  Node(const Node &);
  Node& operator=(const Node &);
  
  void setParent(Node* parent);

  void updateTransform(const Matrix43 &mat);

  NodeContent *content_;
  
  Node *parent_;
  std::list<Node*> childs_;

  bool visible_;
};


} // namespace ewok
  

#endif  // NODE_H_
