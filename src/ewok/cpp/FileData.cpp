#include "FileData.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include "Log.h"


namespace ewok {


FileData::FileData() : size_(0), bytes_(NULL) {
}


FileData::FileData(const char *filename) : size_(0), bytes_(NULL) {
  read(filename);
}


FileData::~FileData() {
  delete[] bytes_;
}

  
void FileData::read(const char *filename) {
  LogInfo("reading file %s", filename);
  assert(size_ == 0 && bytes_ == NULL);
  
  FILE *f = fopen(filename, "rb");
  if (!f) {
	LogError("cannot open file %s", filename);
	fclose(f);
	return;
  }
  
  fseek(f, 0, SEEK_END);
  size_ = ftell(f);
  rewind(f);
  
  bytes_ = new char[size_];
  size_t result = fread(bytes_, 1, size_, f);
  if (!result) {
	LogError("cannot read file %s", filename);
	size_ = 0;
  }

  fclose(f);
}


size_t FileData::size() const {
  return size_;
}


const char *FileData::bytes() const {
  return bytes_;
}


} // namespace ewok
