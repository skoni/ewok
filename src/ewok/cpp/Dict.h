#ifndef DICT_H_
#define DICT_H_


#include <cstring>
#include <cassert>
#include "Hash.h"



namespace ewok {

template<typename T> struct DictEntry;
template<typename T> class DictIter;

  
template<typename T>
class Dict {
  friend class DictIter<T>;
  
 public:
  Dict(unsigned numBuckets=31);
  Dict(const Dict &dict);
  ~Dict();

  Dict<T>& operator=(const Dict<T> &dict);  
  
  T set(const char *name, T &value);
  T get(const char *name) const;

  T remove(const char *name);
  void removeAll();
  
  bool contains(const char *name) const;

  DictIter<T> iter();
  const DictIter<T> iter() const;

 private:
  unsigned getHash(const char *name) const;
  void initBuckets();
  
  DictEntry<T> **buckets_;
  const unsigned numBuckets_;
};


template<typename T>
Dict<T>::Dict(unsigned numBuckets)
  : buckets_(NULL),
    numBuckets_(numBuckets)
{
  initBuckets();
}


template<typename T>
Dict<T>::Dict(const Dict &dict)
  : buckets_(NULL),
    numBuckets_(dict.numBuckets_)
{
  initBuckets();
  
  DictIter<T> i = dict.iter();
  while(i.moveNext()) {
    set(i.name(), i.value());
  }
}


template<typename T>
Dict<T>::~Dict() {
  removeAll();
}


template<typename T>
Dict<T> &Dict<T>::Dict::operator=(const Dict<T> &dict) {
  DictIter<T> i = dict.iter();
  while(i.moveNext()) {
    set(i.name(), i.value());
  }
  return *this;
}


template<typename T>
void Dict<T>::initBuckets() {
  assert(!buckets_);
  buckets_ = new DictEntry<T>*[numBuckets_];
  memset(buckets_, 0, sizeof(DictEntry<T>*) * numBuckets_);
}


template<typename T>
T Dict<T>::set(const char *name, T &value) {
  unsigned hash = getHash(name);

  /* if (buckets_[hash] != NULL) */
  /* 	LogInfo("collision with name: %s and %s", name, buckets_[hash]->name); */
  
  DictEntry<T> *newEntry = new DictEntry<T>(name, value, buckets_[hash]);
  buckets_[hash] = newEntry;
  return newEntry->value;
}


template<typename T>
T Dict<T>::get(const char *name) const {
  unsigned hash = getHash(name);

  if (buckets_[hash]) {
	DictEntry<T> *entry = buckets_[hash];

	while (entry) {
	  if (strcmp(name, entry->name) == 0) {
		return entry->value;
	  }
	  entry = entry->next;
	}
  }
  return NULL;
}


template<typename T>
T Dict<T>::remove(const char *name) {
  unsigned hash = getHash(name);

  if (buckets_[hash] != NULL) {
	DictEntry<T> *entry = buckets_[hash];
	DictEntry<T> *prev = NULL;

	while (entry) {
	  if (strcmp(name, entry->name) == NULL) {
		
		if (buckets_[hash] == entry) {
		  buckets_[hash] = entry->next;
		}
		else {
		  if (prev) {
			prev->next = entry->next;
		  }
		}
		T data = entry->value;
		delete entry;
		
		return data;
	  }
	  prev = entry;
	  entry = entry->next;
	}
  }
  return NULL;
}


template<typename T>
void Dict<T>::removeAll() {
  for (unsigned i = 0; i < numBuckets_; ++i) {
	if (buckets_[i]) {
	  DictEntry<T> *entry = buckets_[i];
	  DictEntry<T> *next = entry->next;
	  do {
		next = entry->next;
		delete entry;
		entry = next;
	  }
	  while (entry);
	}
  }
}


template<typename T>
bool Dict<T>::contains(const char *name) const {
  unsigned hash = getHash(name);

  if (buckets_[hash]) {
	DictEntry<T> *entry = buckets_[hash];

	while (entry) {
	  if (strcmp(name, entry->name) == 0) {
		return true;
	  }
	  entry = entry->next;
	}
  }
  return false;
}


template<typename T>
inline
DictIter<T> Dict<T>::iter() {
  return DictIter<T>(this);
}


template<typename T>
inline
const DictIter<T> Dict<T>::iter() const {
  return DictIter<T>(this);
}


template<typename T>
unsigned Dict<T>::getHash(const char *name) const {
  return Hash((unsigned char *)name) % numBuckets_;
}


// ----------------------------------------------------------------------
// DictEntry
// ----------------------------------------------------------------------


template<typename T>
struct DictEntry {
  T value;
  const char *name;
  DictEntry *next;

  DictEntry(const char *_name, T &_value, DictEntry *_next) {
	name = _name;
	value = _value;
	next = _next;
  }

  DictEntry(const char *_name, DictEntry *_next) {
	name = _name;
	next = _next;
  }
};


// ----------------------------------------------------------------------
// DictIter
// ----------------------------------------------------------------------


template<typename T>
class DictIter {
  friend class Dict<T>;
  
public:
  bool moveNext() const;

  const char *name() const;
  T value() const;

  void reset() const;

private:
  DictIter(Dict<T> *dict)
    : dict_(dict),
      current_(NULL),
      bucketIndex_(0) {}

  // DictIter(const Dict<T> *dict) : current_(NULL), bucketIndex_(0) {
  //   dict_ = const_cast< Dict<T>* >(dict);
  // }

  Dict<T> *dict_;
  mutable DictEntry<T> *current_;
  mutable unsigned bucketIndex_;
};


template<typename T>
inline
bool DictIter<T>::moveNext() const {
  if (!current_) {
    current_ = dict_->buckets_[0];
    return current_ != NULL;
  }
  else if (current_->next == NULL) {
    if (bucketIndex_+1 < dict_->numBuckets_
	&& dict_->buckets_[bucketIndex_+1] != NULL) {
      ++bucketIndex_;
      current_ = dict_->buckets_[bucketIndex_];
      return true;
    }
    else {
      return false;
    }
  }
  else {
    current_ = current_->next;
    return true;
  }
}


template<typename T>
const char *DictIter<T>::name() const {
  assert(current_);
  return current_->name;
}


template<typename T>
inline
T DictIter<T>::value() const {
  assert(current_);
  return current_->value;
}


template<typename T>
void DictIter<T>::reset() const {
  current_ = NULL;
  bucketIndex_ = 0;
}


} // namespace ewok

  
#endif  // DICT_H_
