#ifndef GAME_H_
#define GAME_H_


#include "Application.h"


void app_hello(ewok::Application *app);


void EwokBridgeInit(ewok::Application *app);
void EwokBridgeDeinit(ewok::Application *app);

void EwokBridgeUpdate(ewok::Application *app, double time);


#endif  // GAME_H_
