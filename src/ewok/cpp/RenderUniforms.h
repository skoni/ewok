#ifndef RENDERUNIFORMS_H_
#define RENDERUNIFORMS_H_


#include <cassert>
#include "Vector.h"


namespace ewok {


class RenderProgram;


class RenderUniforms {
  friend class RenderProgram;
  
public:
  void set1f(const char *name, float x);
  void set1fv(const char *name, unsigned count, const float *v);
  
  void set1i(const char *name,  int x);
  void set1iv(const char *name, unsigned count, const int *v);

  void set2f(const char *name, const Vector2 &v);
  void set2fv(const char *name, unsigned count, const Vector2 *v);
  
  void set2i(const char *name, const Vector2i &v);
  void set2iv(const char *name, unsigned count, const Vector2i *v);

  void set3f(const char *name, const Vector3 &v);
  void set3fv(const char *name, unsigned count, const Vector3 *v);
  
  void set3i(const char *name, const Vector3i &v);
  void set3iv(const char *name, unsigned count, const Vector3i *v);

  void set4f(const char *name, const Vector4 &v);
  void set4fv(const char *name, unsigned count, const Vector4 *v);
  
  void set4i(const char *name, const Vector4i &v);
  void set4iv(const char *name, unsigned count, const Vector4i *v);

  // void glUniformMatrix2fv(GLint location, GLsizei count,
  // 			  GLboolean transpose, const GLfloat* value);
  // void glUniformMatrix3fv(GLint location, GLsizei count,
  // 			  GLboolean transpose, const GLfloat* value);
  // void glUniformMatrix4fv(GLint location, GLsizei count,
  // 			  GLboolean transpose, const GLfloat* value);
  
private:
  RenderUniforms(unsigned programId);
  
  int getUniformLocation(const char *name);

  unsigned programId_;
};


} // namespace ewok


#endif  // RENDERUNIFORMS_H_
