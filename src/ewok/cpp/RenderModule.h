#ifndef RENDERMODULE_H_
#define RENDERMODULE_H_


#include "RenderProgram.h"
#include "RenderBuffer.h"
#include "RenderAttribs.h"
#include "Shader.h"
#include "Texture.h"


namespace ewok {


// -----------------------------------------------------------------------------
// RenderModule
// -----------------------------------------------------------------------------


class RenderModule {
public:
  virtual void loadShaders(const char *vshaderName, const char *fshaderName);

  virtual void addAttrib(const char *name, RenderAttrib::Type type, int numComps);

  virtual RenderUniforms use();

  virtual Texture loadTexture(const char *name);

protected:
  virtual void init();

  virtual void render(const VertexBuffer &vbuffer, const IndexBuffer &ibuffer, unsigned numIndices);

  virtual void render(const VertexBuffer &vbuffer, unsigned startIndex, unsigned endIndex);

private:
  Shader vshader_;
  Shader fshader_;

  RenderProgram program_;
  RenderAttribs attribs_;
};


// -----------------------------------------------------------------------------
// VertexRenderModule
// -----------------------------------------------------------------------------


class VertexRenderModule : public RenderModule {
public:
  void init(VertexBuffer::ElementType elemType=VertexBuffer::TRIANGLES);

  void render(unsigned startIndex, unsigned endIndex);

  void writeVertexBuffer(size_t size, const unsigned char *data);
  
private:
  VertexBuffer vbuffer_;
};


// -----------------------------------------------------------------------------
// IndexRenderModule
// -----------------------------------------------------------------------------


class IndexRenderModule : public RenderModule {
public:
  void init(VertexBuffer::ElementType elemType=VertexBuffer::TRIANGLES,
	      IndexBuffer::IndexType indexType=IndexBuffer::USHORT);

  void render(unsigned numIndices);

  void writeVertexBuffer(size_t size, const unsigned char *data);

  void writeIndexBuffer(size_t size, const unsigned char *data);
  
protected:
  VertexBuffer vbuffer_;
  IndexBuffer ibuffer_;
};


} // namespace ewok


#endif // RENDERMODULE_H_
