#include "RenderBuffer.h"

#include <cassert>


namespace ewok {


/// TODO, use glMapBuffer, cause video and cpu ram are the same !!!


// -----------------------------------------------------------------------------
// RenderBuffer
// -----------------------------------------------------------------------------


RenderBuffer::RenderBuffer()
  : type_(VERTEX_BUFFER),
    usage_(STATIC_DRAW)
{
}


RenderBuffer::~RenderBuffer() {
  release();
}


void RenderBuffer::init(Type type, Usage usage) {
  type_ = type;
  usage_ = usage;
  
  glGenBuffers(1, &id_);
  assert(id_);
  CheckGLError();
}


void RenderBuffer::bind() const {
  assert(id_);
  glBindBuffer(type_, id_);
  CheckGLError();  
}


void RenderBuffer::write(size_t size, const unsigned char *data) {
  assert(id_);
  glBindBuffer(type_, id_);
  glBufferData(type_, size, data, usage_);
  CheckGLError();
}


void RenderBuffer::release() {
  if (id_) {
    glBufferData(type_, 0, NULL, usage_);
    glDeleteBuffers(1, &id_);
    CheckGLError();
  }
}


// -----------------------------------------------------------------------------
// VertexBuffer
// -----------------------------------------------------------------------------


void VertexBuffer::init(RenderBuffer::Usage usage, ElementType type) {
  RenderBuffer::init(RenderBuffer::VERTEX_BUFFER, usage);
  elementType_ = type;
}


VertexBuffer::ElementType VertexBuffer::elementType() const {
  return elementType_;
}


// -----------------------------------------------------------------------------
// IndexBuffer
// -----------------------------------------------------------------------------


void IndexBuffer::init(RenderBuffer::Usage usage, IndexType type) {
  RenderBuffer::init(RenderBuffer::INDEX_BUFFER, usage);
  indexType_ = type;
}

IndexBuffer::IndexType IndexBuffer::indexType() const {
  return indexType_;
}


} // namespace ewok
