#ifndef SCENE_H_
#define SCENE_H_

#include "Node.h"


namespace ewok {


class Scene {
 public:
  Scene();

  Node *rootNode();

  void update();

 private:
  Matrix43 transform_;
  Node rootNode_;
};


} // namespace ewok

#endif  // SCENE_H_
