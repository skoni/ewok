#include "RenderProgram.h"

#include "OpenGL.h"
#include "RenderBuffer.h"


namespace ewok {


RenderProgram::RenderProgram() {
}


RenderProgram::~RenderProgram() {
  glDetachShader(id_, vshaderId_);
  glDetachShader(id_, fshaderId_);  
  glDeleteProgram(id_);
  CheckGLError();
}


void RenderProgram::init(unsigned vshaderId, unsigned fshaderId,
			 const RenderAttribs &attribs) {
  assert(vshaderId);
  assert(fshaderId);
  vshaderId_ = vshaderId;
  fshaderId_ = fshaderId;
  attribs_ = attribs;
  link();
}


void RenderProgram::link() {
  id_ = glCreateProgram();
  if (!id_) {
	LogError("cannot create program");
	return;
  }

  attribs_.bindLocations(id_);
  
  glAttachShader(id_, vshaderId_);
  glAttachShader(id_, fshaderId_);

  glLinkProgram(id_);

  GLint success = 0;
  glGetProgramiv(id_, GL_LINK_STATUS, &success);
  if (!success) {
	GLint infoLen = 0;
	glGetProgramiv(id_, GL_INFO_LOG_LENGTH, &infoLen);

	if (infoLen > 1) {
	  char *infoLog = new char[infoLen];
	  glGetProgramInfoLog(id_, infoLen, 0, infoLog);
	  LogError(infoLog);
	  delete []infoLog;
	}
	glDeleteProgram(id_);
	return;
  }
  LogShaderVars(id_);
}


RenderUniforms RenderProgram::use() {
  glUseProgram(id_);
  CheckGLError();
  return RenderUniforms(id_);
}


void RenderProgram::render(const VertexBuffer &vertexBuffer,
			   const IndexBuffer &indexBuffer,
			   unsigned numIndices) {
  vertexBuffer.bind();
  indexBuffer.bind();

  attribs_.enable();

  glDrawElements(vertexBuffer.elementType(), numIndices, indexBuffer.indexType(), NULL);
  CheckGLError();
  
  attribs_.disable();
}


void RenderProgram::render(const VertexBuffer &vertexBuffer,
			   unsigned start, unsigned num) {
  vertexBuffer.bind();
  CheckGLError();
  
  attribs_.enable();

  glDrawArrays(vertexBuffer.elementType(), start, num);
  CheckGLError();

  attribs_.disable();
}


} // namespace ewok
