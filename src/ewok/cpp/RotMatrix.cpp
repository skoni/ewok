#include "RotMatrix.h"

#include <cmath>
#include "Vector.h"


namespace ewok {


RotMatrix::RotMatrix(const Vector3 &orient) {
  setOrientation(orient);
}


void RotMatrix::setOrientation(const Vector3 &orient) {
  float sx = sin(orient.x); float cx = cos(orient.x);
  float sy = sin(orient.y); float cy = cos(orient.y);
  float sz = sin(orient.z); float cz = cos(orient.z);

  m11 = cy*cz;             m12 = cy*sz;             m13 = -sy;
  m21 = sx*sy*cz - cx*sz;  m22 = sx*sy*sz + cx*cz;  m23 = sx*cy;
  m31 = cx*sy*cz + sx*sz;  m32 = cx*sy*sz - sx*cz;  m33 = cx*cy;
}


// functions --------------------


// RotMatrix RotMatrixX(float t) {
//   float c = cos(t);
//   float s = sin(t);
//   return RotMatrix(1.0f, 0.0f,  0.0f,
// 		0.0f, c,     s,
// 		0.0f, -s,    c);
// }


// RotMatrix RotMatrixY(float t) {
//   float c = cos(t);
//   float s = sin(t);
//   return RotMatrix(c,    0.0f, -s,
// 		0.0f, 1.0f,  0.0f,
// 		s,    0.0f,  c);
// }


// RotMatrix RotMatrixZ(float t) {
//   float c = cos(t);
//   float s = sin(t);
//   return RotMatrix( c,    s,    0.0f,
// 		-s,    c,    0.0f,
// 		 0.0f, 0.0f, 1.0f);
// }


// RotMatrix RotMatrixXYZ(float x, float y, float z) {
//   float sx = sin(x); float cx = cos(x);
//   float sy = sin(y); float cy = cos(y);
//   float sz = sin(z); float cz = cos(z);
//   return RotMatrix(cy*cz,             cy * sz,          -sy,
// 	 sx*sy*cz - cx*sz,  sx*sy*sz + cx*cz,  sx*cy,
// 	 cx*sy*cz + sx*sz,  cx*sy*sz - sx*cz,  cx*cy);
// }


} // namespace ewok
