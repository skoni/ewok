#include "Mesh.h"


namespace ewok {


template<typename T>
Mesh<T>::Mesh() {
}


template<typename T>
Mesh<T>::~Mesh() {
}


template<typename T>
void Mesh<T>::setVertices(const Array<T> &vertices) {
  vertices_ = vertices;
}


template<typename T>
const Array<T> *Mesh<T>::vertices() const {
  return &vertices_;
}


template<typename T>
const unsigned char *Mesh<T>::vertexData() const {
  return vertices_.bytes();
}


template<typename T>
size_t Mesh<T>::vertexDataSize() const {
  return vertices_.sizeInBytes();
}


template<typename T>
size_t Mesh<T>::vertexDataStride() const {
  return sizeof(T);
}


} // ewok
