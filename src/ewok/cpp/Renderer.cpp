#include "Renderer.h"

#include "OpenGL.h"
#include "Renderable.h"
#include "RenderTest.h"


namespace ewok {


Renderer::Renderer() {
}


Renderer::~Renderer() {
  std::vector<Renderable*>::iterator it;
  for (it = renderables_.begin(); it != renderables_.end(); it++) {
    delete *it;
  }
  renderables_.clear();
}


void Renderer::init() {
  LogOpenGLVars();

  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glViewport(0, 0, 640, 480);

  glDepthRange(0.0f, 1.0f);

  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW);

  // enable point sprites
  glEnable(GL_POINT_SPRITE);
  glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
  

  // rendertestssssss.....
  addRenderable(new RenderTestTexture());
  addRenderable(new RenderTestPointSprites());
  addRenderable(new RenderTestLines());
  addRenderable(new PointSpriteContainer());
}


void Renderer::deinit() {
}


void Renderer::addRenderable(Renderable *r) {
  renderables_.push_back(r);
}


void Renderer::render(double time) {
  glClear(GL_COLOR_BUFFER_BIT);

  std::vector<Renderable*>::iterator it;
  for (it = renderables_.begin(); it != renderables_.end(); it++) {
    (*it)->render(time);
  }
}


} // namespace ewok
