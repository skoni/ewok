#ifndef ARRAY_H_
#define ARRAY_H_

#include <cassert>
#include <cstring>


namespace ewok {


template<typename T>class ArrayIter;


// -----------------------------------------------------------------------------
// Array
// -----------------------------------------------------------------------------

template<typename T>
class Array {
  friend class ArrayIter<T>;

public:
  Array(unsigned maxLength=32);

  T first() const;
  T last() const;

  T& append(const T &data);
  void appendArray(unsigned numItems, const T arr[]);
  
  T& get(unsigned index) const;
  
  void set(unsigned index, const T &data);

  void removeAll();

  size_t length() const;
  size_t maxLength() const;

  size_t size() const;
  const unsigned char *bytes() const;

  ArrayIter<T> iter();
  const ArrayIter<T> iter() const;

private:
  void adjustSize(unsigned requestedIndex);
  
  T* items_;
  size_t length_;
  size_t maxLength_;
};


template<typename T>
Array<T>::Array(unsigned maxLength)
  : length_(0),
    maxLength_(maxLength)
{
  items_ = (T*)calloc(maxLength, sizeof(T));
}


template<typename T>
T Array<T>::first() const {
  return length_ ? &items_[0] : 0;
}


template<typename T>
T Array<T>::last() const {
  return length_ ? &items_[length_ - 1] : 0;
}


template<typename T>
void Array<T>::adjustSize(unsigned requestedIndex) {
  if (requestedIndex >= maxLength_) {
    unsigned newMaxLen = maxLength_ - requestedIndex % 32 + 32;
    T* newItems = (T*)malloc(sizeof(T) * newMaxLen);
    memcpy(newItems, items_, sizeof(T) * length_);
    memset(newItems + length_, 0, newMaxLen - length_);
    
    free(items_);
    items_ = newItems;
    maxLength_ = newMaxLen;
  }
}


template<typename T>
T& Array<T>::append(const T &data) {
  adjustSize(length_);
  items_[length_] = data;
  ++length_;
  return items_[length_-1];
}


template<typename T>
void Array<T>::appendArray(unsigned numItems, const T arr[]) {
  adjustSize(length_ + numItems - 1);
  memcpy(items_ + length_, arr, sizeof(T) * numItems);
  length_ += numItems;
}


template<typename T>
inline
T& Array<T>::get(unsigned index) const {
  assert(index < length_);
  return items_[index];
}


template<typename T>
inline
void Array<T>::set(unsigned index, const T &data) {
  // adjustSize(index);
  // items_[index] = data;
  // if (length_ <= index) {
  //   length_ = index + 1;
  // }
  assert(index < length_);
  items_[index] = data;
}


template<typename T>
void Array<T>::removeAll() {
  length_ = 0;
  free(items_);
  items_ = (T*)calloc(32, sizeof(T));
}


template<typename T>
inline
size_t Array<T>::length() const {
  return length_;
}


template<typename  T>
size_t Array<T>::maxLength() const {
  return maxLength_;
}


template<typename T>
size_t Array<T>::size() const {
  return sizeof(T) * length_;
}


template<typename T>
const unsigned char *Array<T>::bytes() const {
  return (unsigned char *)items_;
}


template<typename T>
inline
ArrayIter<T> Array<T>::iter() {
  return ArrayIter<T>(this);
}


template<typename T>
inline
const ArrayIter<T> Array<T>::iter() const {
  return ArrayIter<T>(this);
}


// -----------------------------------------------------------------------------
// ArrayIter
// -----------------------------------------------------------------------------

template<typename T>
class ArrayIter {
  friend class Array<T>;
  
public:
  bool moveNext() const;
  bool movePrev() const;
  
  T& current() const;
  long index() const;

  void reset() const;

  void set(T value);

private:
  ArrayIter(Array<T> *array) : array_(array), current_(-1) {}

  ArrayIter(const Array<T> *array) : current_(-1) {
    array_ = const_cast< Array<T>* >(array);
  }

  Array<T> *array_;
  mutable long current_;
};


template<typename T>
inline
bool ArrayIter<T>::moveNext() const {
  if (array_->length_ == 0) {
    return false;
  }
  else if (current_ == -1) {
    current_ = 0;
    return true;
  }
  else if (current_ + 1 == array_->length_) {
    return false;
  }
  else {
    ++current_;
    return true;
  }
}

template<typename T>
inline
bool ArrayIter<T>::movePrev() const {
  if (array_->length == 0) {
    return false;
  }
  else if (current_ == -1) {
    current_ = array_->length_ - 1;
    return true;
  }
  else if (current_ == 0) {
    return false;
  }
  else {
    --current_;
    return true;
  }
}


template<typename T>
inline
T& ArrayIter<T>::current() const {
  assert(current_ != -1);
  return array_->items_[current_];
}


template<typename T>
inline
long ArrayIter<T>::index() const {
  assert(current_ != -1);
  return current_;
}


template<typename T>
inline
void ArrayIter<T>::set(T value) {
  assert(current_ != -1);
  array_->items_[current_] = value;
}


template<typename T>
void ArrayIter<T>::reset() const {
  current_ = -1;
}

  
} // namespace ewok

#endif  // ARRAY_H_
