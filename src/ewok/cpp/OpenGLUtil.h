#ifndef OPENGLUTIL_H_
#define OPENGLUTIL_H_

#include "OpenGL.h"
#include "Log.h"


namespace ewok {


const char* GLErrorName(GLenum error);
const char* GLTypename(GLenum type);

void LogShaderVars(GLuint program);
void LogOpenGLVars();

void CheckGLError();


} // namespace ewok


#endif  // OPENGLUTIL_H_
