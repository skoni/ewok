#include "Node.h"

#include <cassert>


namespace ewok {


// -----------------------------------------------------------------------------
// NodeContent
// -----------------------------------------------------------------------------


Node *NodeContent::node() {
  return node_;
}


void NodeContent::setNode(Node *node) {
  node_ = node;
}


// -----------------------------------------------------------------------------
// NodeTransform
// -----------------------------------------------------------------------------


const Matrix43 NodeTransform::update(const Matrix43 &parent) {
  RotMatrix rotmat(rotation);
  Matrix43 local(rotmat, position);

  transform_ = local * parent;
  return transform_;
}


inline
const Matrix43 NodeTransform::transform() {
  return transform_;
}



// -----------------------------------------------------------------------------
// Node
// -----------------------------------------------------------------------------


Node::Node(NodeContent *content)
  : content_(NULL), parent_(NULL), visible_(true)
{
  if (content_) {
    content_ = content;
    content_->setNode(this);
  }
}


Node::~Node() {
  if (content_) delete content_;
  std::list<Node*>::iterator it;
  for (it = childs_.begin(); it != childs_.end(); ++it) {
    delete(*it);
  }
}


Node *Node::parent() const {
  return parent_;
}


bool Node::operator ==(const Node &other) {
  return this == &other;
}


void Node::setParent(Node* parent) {
  parent_ = parent;
}


void Node::addChild(Node *node) {
  assert(node->parent() == NULL);
  node->setParent(this);
  childs_.push_back(node);
}


void Node::removeChild(Node *node) {
  node->setParent(NULL);
  childs_.remove(node);
}


const std::list<Node*> *Node::childs() {
  return &childs_;
}


bool Node::visible() {
  return visible_;
}


void Node::setVisible(bool vis) {
  visible_ = vis;
}


void Node::updateTransform(const Matrix43 &parent) {
  Matrix43 local = transform.update(parent);

  std::list<Node*>::iterator it;
  for (it = childs_.begin(); it != childs_.end(); ++it) {
    (*it)->updateTransform(local);
  }
}


} // namespace
