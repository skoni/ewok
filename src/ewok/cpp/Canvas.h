#ifndef CANVAS_H_
#define CANVAS_H_

#include <Cocoa/Cocoa.h>


class CanvasText;


// Canvas for premultiplied RGBA images
class Canvas {
  friend class CanvasText;
  
public:
  Canvas(unsigned width, unsigned height, void *data)
    : width_(width), height_(height)
  {
    unsigned bitsPerComponent = 8;
    unsigned bytesPerRow = width * 4;

    // only premultiplied alpha is supported with CGBitmapContext
    CGBitmapInfo info = kCGImageAlphaPremultipliedLast;
    
    colorspace_ = CGColorSpaceCreateDeviceRGB();
    context_ = CGBitmapContextCreate(data, width, height,
				     bitsPerComponent, bytesPerRow,
				     colorspace_, info);
  }


  ~Canvas() {
    CGColorSpaceRelease(colorspace_);
    CGContextRelease(context_);
  }


  void setFillColor(float r, float g, float b, float a) {
    CGContextSetRGBFillColor(context_, r, g, b, a);
  }


  void fillRect(int x, int y, int w, int h) {
    CGContextFillRect(context_, CGRectMake(x, height_ - y - h, w, h));
  }


  CGContextRef context() {
    return context_;
  }

private:

  unsigned width_;
  unsigned height_;
  
  CGContextRef context_;
  CGColorSpaceRef colorspace_;
};





#endif  // CANVAS_H_
