#ifndef TEXTURE_H_
#define TEXTURE_H_


namespace ewok {

class Image;


class Texture {
public:
  Texture();
  ~Texture();

  void init(const Image &img, bool createMipmaps);
  void deinit();

  void activate(unsigned texUnitNum);

  int width();
  int height();
    
private:
  int width_;
  int height_;
  unsigned id_;
};


} // namespace ewok


#endif  // TEXTURE_H_
