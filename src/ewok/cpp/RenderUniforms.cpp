#include "RenderUniforms.h"

#include "RenderProgram.h"


namespace ewok {


RenderUniforms::RenderUniforms(unsigned programId)
  : programId_(programId) {
}


int RenderUniforms::getUniformLocation(const char *name) {
  assert(programId_);
  int location = glGetUniformLocation(programId_, name);
  if (location == -1) {
    // uniform no present or optimized away
    LogWarn("glGetUniformLocation does not find %s", name);
  }
  return location;
}


void RenderUniforms::set1f(const char *name, float x) {
  glUniform1f(getUniformLocation(name), x);
}
void RenderUniforms::set1fv(const char *name, unsigned count, const float *v) {
  glUniform1fv(getUniformLocation(name), count, v);
}


void RenderUniforms::set1i(const char *name, int x) {
  glUniform1i(getUniformLocation(name), x);
}
void RenderUniforms::set1iv(const char *name, unsigned count, const int *v) {
  glUniform1iv(getUniformLocation(name), count, v);
}


void RenderUniforms::set2f(const char *name, const Vector2 &v) {
  glUniform2f(getUniformLocation(name), v.x, v.y);
}
void RenderUniforms::set2fv(const char *name, unsigned count, const Vector2 *v) {
  glUniform2fv(getUniformLocation(name), count, (const float *)v);
}


void RenderUniforms::set2i(const char *name, const Vector2i &v) {
  glUniform2i(getUniformLocation(name), v.x, v.y);
}
void RenderUniforms::set2iv(const char *name, unsigned count, const Vector2i *v) {
  glUniform2iv(getUniformLocation(name), count, (const int *)v);
}


void RenderUniforms::set3f(const char *name, const Vector3 &v) {
  glUniform3f(getUniformLocation(name), v.x, v.y, v.z);
}
void RenderUniforms::set3fv(const char *name, unsigned count, const Vector3 *v) {
  glUniform3fv(getUniformLocation(name), count, (const float *)v);
}


void RenderUniforms::set3i(const char *name, const Vector3i &v) {
  glUniform3i(getUniformLocation(name), v.x, v.y, v.z);
}
void RenderUniforms::set3iv(const char *name, unsigned count, const Vector3i *v) {
  glUniform3iv(getUniformLocation(name), count, (const int *)v);
}

void RenderUniforms::set4f(const char *name, const Vector4 &v) {
  glUniform4f(getUniformLocation(name), v.x, v.y, v.z, v.w);
}
void RenderUniforms::set4fv(const char *name, unsigned count, const Vector4 *v) {
  glUniform4fv(getUniformLocation(name), count, (const float *)v);
}


void RenderUniforms::set4i(const char *name, const Vector4i &v) {
  glUniform4i(getUniformLocation(name), v.x, v.y, v.z, v.w);
}
void RenderUniforms::set4iv(const char *name, unsigned count, const Vector4i *v) {
  glUniform4iv(getUniformLocation(name), count, (const int *)v);
}


} // namespace ewok

