#include "Application.h"

#include "Bridge.h"
#include "Log.h"


namespace ewok {
  

Application::Application()
  : fps_(0),
    lastTime_(0)
{
}


void Application::init(int width, int height) {
  renderer_.init();
  EwokBridgeInit(this);
}


void Application::update(double time) {
  EwokBridgeUpdate(this, time);
  scene_.update();
  renderer_.render(time);

  // FPS
  if (time - lastTime_ >= 1.0) {
    LogInfo("FPS: %d", fps_);
    lastTime_ = time;
    fps_ = 0;
  }
  else {
    ++fps_;
  }
}


void Application::deinit() {
  EwokBridgeDeinit(this);
  renderer_.deinit();
}


Scene *Application::scene() {
  return &scene_;
}


} // namespace ewok
