#ifndef FILEDATA_H_
#define FILEDATA_H_

#include <cstddef>


namespace ewok {


class FileData {
public:
  FileData();
  FileData(const char *filename);
  ~FileData();

  void read(const char *filename);

  size_t size() const;
  const char *bytes() const;
  
private:
  FileData(const FileData &);
  FileData& operator=(const FileData &);

  size_t size_;
  char *bytes_;
};


} // namespace ewok


#endif  // FILEDATA_H_
