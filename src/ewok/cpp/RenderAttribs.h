#ifndef RENDERATTRIBS_H_
#define RENDERATTRIBS_H_

#include <vector>
#include "OpenGL.h"


namespace ewok {


class RenderAttribs;


// -----------------------------------------------------------------------------
// RenderAttrib
// -----------------------------------------------------------------------------


class RenderAttrib {
  friend class RenderAttribs;
  
public:
  enum Type {
    BYTE = GL_BYTE,
    UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
    SHORT = GL_SHORT,
    UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
    INT = GL_INT,
    UNSIGNED_INT = GL_UNSIGNED_INT,
    FLOAT = GL_FLOAT,
    DOUBLE = GL_DOUBLE
  };

  RenderAttrib();
  RenderAttrib(const char *name,
	       Type compType, 
	       unsigned numComps,
	       unsigned index);

private:
  const char *name;
  Type compType;
  unsigned numComps;
  unsigned compSize;
  unsigned index;
};


// -----------------------------------------------------------------------------
// RenderAttribs
// -----------------------------------------------------------------------------


class RenderAttribs {
public:
  RenderAttribs();
  ~RenderAttribs();

  void append(const char *name, RenderAttrib::Type compType, unsigned numComps);

  void bindLocations(unsigned programId);
  void enable();
  void disable();
  
private:
  unsigned nextIndex_;
  unsigned stride_;
  std::vector<RenderAttrib> attribs_;
};


} // namespace ewok


#endif  // RENDERATTRIBS_H_
