#include "Image.h"

#include "stb_image.h"
#include "Log.h"


namespace ewok {


Image::Image()
: width_(0),
  height_(0),
  components_(0),
  size_(0),
  pixels_(NULL) {

}


Image::~Image() {
  if (size_ > 0) {
    stbi_image_free(pixels_);
  }
  pixels_ = NULL;
}


void Image::load(const char *filename) {
  LogInfo("loading image %s", filename);
  pixels_ = stbi_load(filename,
  		      &width_,
  		      &height_,
  		      &components_,
  		      0);

  size_ = width_ * height_ * components_ * 4; // 32bit components

  if (!pixels_) {
    LogError(stbi_failure_reason());
    width_ = 0;
    height_ = 0;
    components_ = 0;
    size_ = 0;
  }
}


unsigned Image::width() const {
  return width_;
}

unsigned Image::height() const {
  return height_;
}

unsigned Image::numComponents() const {
  return components_;
}

unsigned Image::size() const {
  return size_;
}

unsigned char *Image::pixels() const {
  return pixels_;
}


} // namespace ewok
