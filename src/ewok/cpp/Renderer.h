#ifndef RENDERER_H_
#define RENDERER_H_


#include <vector>


namespace ewok {


class Renderable;


class Renderer {
public:
  Renderer();
  ~Renderer();

  void init();
  void deinit();

  void addRenderable(Renderable *r);
  
  void render(double time);

private:
  std::vector<Renderable*> renderables_;
};


} // namespace ewok


#endif  // RENDERER_H_
