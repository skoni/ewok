#ifndef CANVASTEXT_H_
#define CANVASTEXT_H_


#include <Cocoa/Cocoa.h>


class Canvas;


class CanvasText {
public:
  CanvasText() {
    length_ = 0;
    string_ = CFAttributedStringCreateMutable(kCFAllocatorDefault, length_);
    font_ = NULL;
  }

  ~CanvasText() {
    CFRelease(string_);
    if (font_) CFRelease(font_);
  }

  void setText(const char *text, unsigned len) {
    CFStringRef textStr = CFStringCreateWithBytes(kCFAllocatorDefault,
						  (const UInt8 *)text, len,
						  kCFStringEncodingUTF8, false);
    CFAttributedStringReplaceString(string_, CFRangeMake(0, length_), textStr);
    CFRelease(textStr);
    length_ = len;
  }


  void setFont(const char *name, unsigned len, unsigned fontSize) {
    if (font_) {
      CFRelease(font_);
    }
    CFStringRef fontStr = CFStringCreateWithBytes(kCFAllocatorDefault,
						  (const UInt8 *)name, len,
						  kCFStringEncodingUTF8, false);

    CTFontRef font_ = CTFontCreateWithName(fontStr, fontSize, NULL);
    CFRelease(fontStr);

    CFAttributedStringSetAttribute(string_, CFRangeMake(0, length_),
				   kCTFontAttributeName, font_);
  }

  void draw(Canvas *canvas, int x, int y) {
    CTLineRef line = CTLineCreateWithAttributedString(string_);

    CGContextSetTextPosition(canvas->context_, x, y);
    CTLineDraw(line, canvas->context_);

    CFRelease(line);
  }
    
private:
  CFMutableAttributedStringRef string_;
  unsigned length_;

  CTFontRef font_;
};





#endif  // CANVASTEXT_H_
