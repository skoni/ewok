#ifndef MATRIX43_H_
#define MATRIX43_H_

#include "RotMatrix.h"
#include "Vector.h"


namespace ewok {


class Matrix43 {
public:
  float m11, m12, m13;
  float m21, m22, m23;
  float m31, m32, m33;
  float tx,  ty,  tz;

  Matrix43()
    : m11(1.0f), m12(0.0f), m13(0.0f),
      m21(0.0f), m22(1.0f), m23(0.0f),
      m31(0.0f), m32(0.0f), m33(1.0f),
      tx(0.0f),  ty(0.0f),  tz(0.0f) {}
  
  Matrix43(float m11, float m12, float m13,
	   float m21, float m22, float m23,
	   float m31, float m32, float m33,
	   float tx,  float ty,  float tz)
    : m11(m11), m12(m12), m13(m13),
      m21(m21), m22(m22), m23(m23),
      m31(m31), m32(m32), m33(m33),
      tx(tx),   ty(ty),   tz(tz) {}

  Matrix43(const RotMatrix &orient, const Vector3 &pos)
    : m11(orient.m11), m12(orient.m12), m13(orient.m13),
      m21(orient.m21), m22(orient.m22), m23(orient.m23),
      m31(orient.m31), m32(orient.m32), m33(orient.m33),
      tx(pos.x), ty(pos.y), tz(pos.z) {}
  
};


Vector3 operator*(const Vector3 &v, const Matrix43 &m);
Vector3 &operator*=(Vector3 &v, const Matrix43 &m);


Matrix43 operator*(const Matrix43 &a, const Matrix43 &b);
Matrix43 &operator*=(Matrix43 &a, const Matrix43 &b);

  
} // namespace ewok
  

#endif  // MATRIX43_H_
