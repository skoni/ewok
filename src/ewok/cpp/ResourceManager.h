#ifndef RESOURCEMANAGER_H_
#define RESOURCEMANAGER_H_

#include <cassert>

#include "Dict.h"
#include "Texture.h"


namespace ewok {


// -----------------------------------------------------------------------------
// Different Resource Managers
// -----------------------------------------------------------------------------

template<typename T> class ResourceManager;

// typedef ResourceManager<Model> ModelManager;
typedef ResourceManager<Texture> TextureManager;


// -----------------------------------------------------------------------------
// ResourceManager
// -----------------------------------------------------------------------------

template<typename T>
class ResourceManager {
 public:
  ResourceManager();
  ~ResourceManager();
  
  void add(T* res);

  T* get(const char *name) const;

  // const Dict<T*> *resource() const;
  
 private:
  Dict<T*> resources_;
};


template<typename T>
ResourceManager<T>::ResourceManager() {
}


template<typename T>
ResourceManager<T>::~ResourceManager() {
  DictIter<T*> i = resources_.iter();
  while(i.moveNext()) {
    delete i.value();
  }
}


template<typename T>
void ResourceManager<T>::add(T* res) {
  assert(!resources_.contains(res->name()));
  resources_.set(res->name(), res);
}


template<typename T>
T* ResourceManager<T>::get(const char *name) const {
  return resources_.get(name);
}


} // namespace ewok

  
#endif  // RESOURCEMANAGER_H_
