#ifndef RENDERTEST_H_
#define RENDERTEST_H_

#include <cmath>
#include <vector>

#include "Renderable.h"
#include "RenderModule.h"
#include "Vertex.h"
#include "Texture.h"
//#include "Array.h"


namespace ewok {


// ----------------------------------------------------------------------
// RenderTestTexture
// ----------------------------------------------------------------------


class RenderTestTexture : public Renderable {
public:
  RenderTestTexture() {
    init();
  }

  
  void init() {
    module_.loadShaders("res/vtex_test.glsl", "res/ftex_test.glsl");

    module_.addAttrib("a_position", RenderAttrib::FLOAT, 3);
    module_.addAttrib("a_texCoord", RenderAttrib::FLOAT, 2);

    module_.init();

    texture_ = module_.loadTexture("res/test.png");

    unsigned numVerts = 5;
    unsigned vi = 0;
    TexVertex vertices[numVerts];
    vertices[vi++] = TexVertex(-0.5f, 0.5f, 0.0f, 0.0f, 0.0f);
    vertices[vi++] = TexVertex(-0.5f, -0.5f, 0.0f, 0.0f, 1.0f);
    vertices[vi++] = TexVertex(0.5f, -0.5f, 0.0f, 1.0f, 1.0f);

    vertices[vi++] = TexVertex(0.5f, -0.5f, 0.0f, 1.0f, 1.0f);
    vertices[vi++] = TexVertex(0.5f, 0.5f, 0.0f, 1.0f, 0.0f);
    
    unsigned short indices[] = {0, 1, 2, 0, 3, 4};
    
    module_.writeVertexBuffer(sizeof(TexVertex) * numVerts, (unsigned char*)vertices);
    module_.writeIndexBuffer(sizeof(unsigned short) * 6, (unsigned char*)indices);
  }

  void render(double ) {
    RenderUniforms unis = module_.use();

    texture_.activate(0);
    unis.set1i("s_texture", 0);

    module_.render(6);
  }
  
private:
  IndexRenderModule module_;
  Texture texture_;
};


// ----------------------------------------------------------------------
// RenderTestLines
// ----------------------------------------------------------------------


class RenderTestLines : public Renderable {
public:
  RenderTestLines() {
    init();
  }

  
  void init() {
    module_.loadShaders("res/vlines_test.glsl", "res/flines_test.glsl");

    module_.addAttrib("a_position", RenderAttrib::FLOAT, 3);
    module_.addAttrib("a_color", RenderAttrib::FLOAT, 4);

    module_.init(VertexBuffer::LINES);

    unsigned numVerts = 6;
    unsigned vi = 0;
    ColorVertex vertices[numVerts];
    vertices[vi++] = ColorVertex(-0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f);
    vertices[vi++] = ColorVertex(-0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f);
    vertices[vi++] = ColorVertex(0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f);

    // vertices[vi++] = ColorVertex(-0.5f, 0.5f, 0.0f, 0.0f, 0.0f);
    vertices[vi++] = ColorVertex(0.5f, -0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f);
    vertices[vi++] = ColorVertex(0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
    
    unsigned short indices[] = {0, 1, 2, 0, 3, 4};

    module_.writeVertexBuffer(sizeof(ColorVertex) * numVerts, (unsigned char*)vertices);
    module_.writeIndexBuffer(sizeof(unsigned short) * 6, (unsigned char*)indices);
  }

  void render(double ) {
    module_.use();
    module_.render(6);
  }
  
private:
  IndexRenderModule module_;
  Texture texture_;
};



// ----------------------------------------------------------------------
// RenderTestPointSprites
// ----------------------------------------------------------------------


class RenderTestPointSprites : public Renderable {
public:
  RenderTestPointSprites() {
    init();
  }
  
  void init() {
    module_.loadShaders("res/vpointsprite_test.glsl", "res/fpointsprite_test.glsl");
    
    module_.addAttrib("a_position", RenderAttrib::FLOAT, 3);
    module_.addAttrib("a_radius", RenderAttrib::FLOAT, 1);

    module_.init(VertexBuffer::POINTS);

    texture_ = module_.loadTexture("res/test.png");

    numVerts_ = 4;
    unsigned vi = 0;
    Vector4 vertices[numVerts_];
    vertices[vi++] = Vector4(-0.5f, 0.5f, 0.0f, 256.0f);
    vertices[vi++] = Vector4(-0.5f, -0.5f, 0.0f, 100.0f);

    vertices[vi++] = Vector4(0.5f, -0.5f, 0.0f, 25.0f);
    vertices[vi++] = Vector4(0.5f, 0.5f, 0.0f, 50.0f);

    module_.writeVertexBuffer(sizeof(Vector4) * numVerts_, (unsigned char*)vertices);
  }


  void render(double ) {
    RenderUniforms unis = module_.use();

    texture_.activate(0);
    unis.set1i("s_texture", 0);

    module_.render(0, numVerts_);
  }


private:
  VertexRenderModule module_;
  Texture texture_;
  unsigned numVerts_;
};


//----------------------------------------------------------------------



class PointSprite {
public:
  PointSprite(float x, float y, float r) : position(x, y), radius(r) {}
  
  Vector2 position;
  float radius;
};


class PointSpriteContainer : public Renderable {
public:
  PointSpriteContainer() {
    init();
  }

  
  void init() {
    module_.loadShaders("res/vsprites.glsl", "res/fsprites.glsl");

    module_.addAttrib("position", RenderAttrib::FLOAT, 2);
    module_.addAttrib("radius", RenderAttrib::FLOAT, 1);

    module_.init(VertexBuffer::POINTS);

    texture_ = module_.loadTexture("res/test.png");

    PointSprite p1(-0.5f, 0.5f, 100.0f);
    // PointSprite p2(-0.5f, -0.5f, 50.0f);
    // PointSprite p3(0.5f, -0.5f, 25.0f);

    sprites_.push_back(p1);
    // sprites_.append(p2);
    // sprites_.append(p3);

    //module_.writeVertexBuffer(sprites_.size(), sprites_.bytes());
    module_.writeVertexBuffer(sizeof(PointSprite) * sprites_.size(),
			      (unsigned char *)&sprites_[0]);
  }


  void update(double time) {
    PointSprite &p1 = sprites_[0];

    float x = cos(time) * 0.5f;
    float y = sin(time) * 0.5f;
    float r = sin(time) * 50.0f + 50.0f;

    p1.position.x = x;
    p1.position.y = y;
    p1.radius = r;

    // module_.writeVertexBuffer(sprites_.size(), sprites_.bytes());
    module_.writeVertexBuffer(sizeof(PointSprite) * sprites_.size(),
			      (unsigned char *)&sprites_[0]);
  }

  
  void render(double time) {
    update(time);
    
    RenderUniforms unis = module_.use();

    texture_.activate(0);
    unis.set1i("texture", 0);

    module_.render(0, sprites_.size());
  }

private:
  VertexRenderModule module_;
  Texture texture_;
  unsigned numVerts_;

  std::vector<PointSprite> sprites_;
};



} // namespace ewok


#endif  // RENDERTEST_H_
