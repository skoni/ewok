#ifndef MESH_H_
#define MESH_H_

#include "Array.h"


namespace ewok {


template<typename T>
class Mesh {
  Mesh();
  ~Mesh();

  void setVertices(const Array<T> &vertices);
  const Array<T> *vertices() const;
  
  const unsigned char *vertexData() const;
  size_t vertexDataSize() const;
  size_t vertexDataStride() const;
  
private:
  Array<T> vertices_;
};


} // namespace ewok



// class Model : public Resource {
// public:
//   Model();
//   virtual ~Model();

//   virtual void init(const char *uri);

//   void addMesh(Mesh *mesh);
//   void addTexture(Texture *tex);
  
// private:
//   List<Mesh*> meshes_;
//   List<Texture*> textures_;
// };





#endif  // MESH_H_
