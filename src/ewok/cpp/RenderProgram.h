#ifndef RENDERPROGRAM_H_
#define RENDERPROGRAM_H_

#include "RenderAttribs.h"
#include "RenderUniforms.h"


namespace ewok {

class Shader;
class VertexBuffer;
class IndexBuffer;


class RenderProgram {
public:
  RenderProgram();
  ~RenderProgram();

  void init(unsigned vshaderId, unsigned fshaderId,
	    const RenderAttribs &attribs);

  // has to be called before rendering
  RenderUniforms use();

  void render(const VertexBuffer &vertBuffer, const IndexBuffer &indexBuffer,
	      unsigned int numIndices);
  
  void render(const VertexBuffer &vertBuffer,
	      unsigned start, unsigned num);
  
  unsigned id() const;
  
private:
  void link();

  unsigned id_;

  unsigned vshaderId_;
  unsigned fshaderId_;

  RenderAttribs attribs_;
};


} // namespace ewok




#endif  // RENDERPROGRAM_H_
