#include "Matrix43.h"


namespace ewok {


// static functions --------------------


Vector3 operator*(const Vector3 &v, const Matrix43 &m) {
  return Vector3(v.x*m.m11 + v.y*m.m21 + v.z*m.m31 + m.tx,
		 v.x*m.m12 + v.y*m.m22 + v.z*m.m32 + m.ty,
		 v.x*m.m13 + v.y*m.m23 + v.z*m.m33 + m.tz);
}


Vector3 &operator*=(Vector3 &v, const Matrix43 &m) {
  v = v * m;
  return v;
}


Matrix43 operator*(const Matrix43 &a, const Matrix43 &b) {
    return Matrix43(a.m11*b.m11 + a.m12*b.m21 + a.m13*b.m31,
                    a.m11*b.m12 + a.m12*b.m22 + a.m13*b.m32,
                    a.m11*b.m13 + a.m12*b.m23 + a.m13*b.m33,

                    a.m21*b.m11 + a.m22*b.m21 + a.m23*b.m31,
                    a.m21*b.m12 + a.m22*b.m22 + a.m23*b.m32,
                    a.m21*b.m13 + a.m22*b.m23 + a.m23*b.m33,

                    a.m31*b.m11 + a.m32*b.m21 + a.m33*b.m31,
                    a.m31*b.m12 + a.m32*b.m22 + a.m33*b.m32,
                    a.m31*b.m13 + a.m32*b.m23 + a.m33*b.m33,

                    a.tx*b.m11 + a.ty*b.m21 + a.tz*b.m31 + b.tx,
                    a.tx*b.m12 + a.ty*b.m22 + a.tz*b.m32 + b.ty,
                    a.tx*b.m13 + a.ty*b.m23 + a.tz*b.m33 + b.tz);
}


Matrix43 &operator*=(Matrix43 &a, const Matrix43 &b) {
  a = a * b;
  return a;
}


} // namespace ewok
