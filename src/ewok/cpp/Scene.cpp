#include "Scene.h"

#include "Log.h"


namespace ewok {


Scene::Scene() {
}


Node *Scene::rootNode() {
  return &rootNode_;
}


void Scene::update() {
  rootNode_.updateTransform(transform_);
}


} // namespace ewok
