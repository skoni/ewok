#ifndef VERTEX_H_
#define VERTEX_H_


#include "Vector.h"
#include "Log.h"


namespace ewok {


struct ColorVertex {
  Vector3 position;
  Vector4 color;
  
  ColorVertex(float x, float y, float z, float r, float g, float b, float a)
    : position(x, y, z), color(r, g, b, a) {
  }

  ColorVertex() {}

  // void printInfo() {
  //   LogInfo("position: %f | %f | %f", position.x, position.y, position.z);
  //   LogInfo("color: %f, %f, %f, %f", color.x, color.y, color.z, color.w);
  // }
};


struct TexVertex {
  Vector3 position;
  Vector2 texCoords;

  TexVertex() {}
  TexVertex(float x, float y, float z, float u, float v)
    : position(x, y, z), texCoords(u, v) {}
};


} // namespace ewok

  
#endif  // VERTEX_H_
