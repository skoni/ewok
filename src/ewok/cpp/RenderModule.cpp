#include "RenderModule.h"


#include "FileData.h"
#include "Image.h"



namespace ewok {


// -----------------------------------------------------------------------------
// RenderModule
// -----------------------------------------------------------------------------


void RenderModule::loadShaders(const char *vshaderName, const char *fshaderName) {
  FileData vf(vshaderName);
  FileData ff(fshaderName);

  vshader_.init(Shader::VERTEX_SHADER, vf.size(), vf.bytes());
  fshader_.init(Shader::FRAGMENT_SHADER, ff.size(), ff.bytes());
}

  
void RenderModule::addAttrib(const char *name, RenderAttrib::Type type, int numComps) {
  attribs_.append(name, type, numComps);
}


Texture RenderModule::loadTexture(const char *name) {
  Image img;
  img.load(name);
  Texture tex;
  tex.init(img, true);
  return tex;
}


RenderUniforms RenderModule::use() {
  return program_.use();
}


void RenderModule::init() {
  program_.init(vshader_.id(), fshader_.id(), attribs_);
}


void RenderModule::render(const VertexBuffer &vbuffer, const IndexBuffer &ibuffer, unsigned numIndices) {
  program_.render(vbuffer, ibuffer, numIndices);
}


void RenderModule::render(const VertexBuffer &vbuffer, unsigned startIndex, unsigned endIndex) {
  program_.render(vbuffer, startIndex, endIndex);
}
  


// -----------------------------------------------------------------------------
// VertexRenderModule
// -----------------------------------------------------------------------------


void VertexRenderModule::init(VertexBuffer::ElementType elemType) {
  RenderModule::init();
  vbuffer_.init(RenderBuffer::DYNAMIC_DRAW, elemType);
}


void VertexRenderModule::render(unsigned startIndex, unsigned endIndex) {
  RenderModule::render(vbuffer_, startIndex, endIndex);
}


void VertexRenderModule::writeVertexBuffer(size_t size, const unsigned char *data) {
  vbuffer_.write(size, data);
}


// -----------------------------------------------------------------------------
// IndexRenderModule
// -----------------------------------------------------------------------------


void IndexRenderModule::init(VertexBuffer::ElementType elemType,
			     IndexBuffer::IndexType indexType)
{
  RenderModule::init();
  vbuffer_.init(RenderBuffer::DYNAMIC_DRAW, elemType);
  ibuffer_.init(RenderBuffer::DYNAMIC_DRAW, indexType);
}


void IndexRenderModule::render(unsigned numIndices) {
  RenderModule::render(vbuffer_, ibuffer_, numIndices);
}


void IndexRenderModule::writeVertexBuffer(size_t size, const unsigned char *data) {
  vbuffer_.write(size, data);
}
  

void IndexRenderModule::writeIndexBuffer(size_t size, const unsigned char *data) {
  ibuffer_.write(size, data);
}


} // namespace ewok

