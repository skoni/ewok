
# ______________________________________________________________________
#
# Manually checkout gambit into ./devel inorder to have the sources for
# compilation.
# ______________________________________________________________________

set(gambc_base_dir ${CMAKE_CURRENT_SOURCE_DIR})
 
set(gambc_src_dir "${gambc_base_dir}/devel")
set(gambc_macos_dir "${gambc_base_dir}/macos")
set(gambc_ios_dir "${gambc_base_dir}/ios")

# update gambc src from the git repo
# configure has to be called first
add_custom_target(
  update_gambc
  COMMAND make bootstrap && make update
  COMMENT "Updating gambc."
  WORKING_DIRECTORY ${gambc_src_dir}
  )

# ______________________________________________________________________
# macos build

add_custom_target(
  configure_gambc_macos
  COMMAND ./configure
  --enable-single-host
  --enable-cplusplus
  --prefix ${gambc_macos_dir}
  --enable-debug
  --enable-profile
  CC=${CMAKE_CXX_COMPILER}
  COMMENT "Configuring gambc for macos use."
  WORKING_DIRECTORY ${gambc_src_dir}
  )

add_custom_target(
  install_gambc_macos
  COMMAND make install
  # DEPENDS configure_gambc_macos
  COMMENT "Installing gambc for macos use."
  WORKING_DIRECTORY ${gambc_src_dir}
  )

# ______________________________________________________________________
# ios build

add_custom_target(
  configure_gambc_ios
  COMMAND ./configure
  --enable-single-host
  --enable-cplusplus
  --prefix ${gambc_ios_dir}
  --enable-debug
  --enable-profile
  CC=${CMAKE_CXX_COMPILER}
  # LD_FLAGS=
  # CC_FLAGS=
  COMMENT "Configuring gambc for ios use."
  WORKING_DIRECTORY ${gambc_src_dir}
  )


add_custom_target(
  install_gambc_ios
  COMMAND make install
  COMMENT "Installing gambc for ios use"
  WORKING_DIRECTORY ${gambc_src_dir}
  )


